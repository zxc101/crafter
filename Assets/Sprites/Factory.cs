﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Factory : MonoBehaviour
{
    public GameObject storage;
    public int level = 1;
    public ETypeSource sourceType;
    public int[] sourceInMin;
    [HideInInspector] public bool isCrafting;

    void Start()
    {
        isCrafting = true;
        StartCoroutine(Craft(name));
    }

    private void OnValidate()
    {
        if (storage != null && !storage.GetComponent<Storage>())
        {
            storage = null;
        }
    }

    IEnumerator Craft(string str)
    {
        while (isCrafting)
        {
            if(sourceInMin.Length >= level)
                storage.GetComponent<Storage>().ChangeValue(sourceType, sourceInMin[level - 1]);

            yield return new WaitForSeconds(1);
        }
    }
}
