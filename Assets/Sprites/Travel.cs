﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class Travel : MonoBehaviour
{
    [SerializeField] private Text timer;
    private bool IsTravel;
    private int time;

    void Start()
    {
        STravel travel = new STravel();
        travel.chance = 0.5f;
        STime t = new STime();
        t.minuts = 0;
        t.second = 3;
        travel.time = t;
        StartCoroutine(StartTravel(travel));
    }

    public IEnumerator StartTravel(STravel travel)
    {
        time = travel.time.minuts * 60 + travel.time.second;
        IsTravel = true;
        while (IsTravel)
        {
            timer.text = GetMinut() + " : " + GetSecond();
            yield return new WaitForSeconds(1);
            if(--time <= 0)
            {
                timer.text = GetMinut() + " : " + GetSecond();
                IsTravel = false;
                if(travel.chance >= Random.value)
                {
                    timer.text = "Удачно";
                }
                else
                {
                    timer.text = "Не удачно";
                }
            }
        }
    }

    public string GetMinut() { return (time/60).ToString(); }
    public string GetSecond() { return (time - (time / 60) * 60).ToString(); }
}
