﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Storage : MonoBehaviour
{
    [SerializeField] private int maxPeople;
    [SerializeField] private int maxMoney;
    [SerializeField] private int maxCrystals;
    [SerializeField] private int maxEnergy;

    private int people;
    private int money;
    private int crystals;
    private int energy;
    private int exp;

    public void ChangeValue(ETypeSource type, int count)
    {
        switch(type)
        {
            case ETypeSource.People:
                if (people + count >= 0 && people + count <= maxPeople)
                    people += count;
                break;
            case ETypeSource.Money:
                if (money + count >= 0 && money + count <= maxMoney)
                    money += count;
                break;
            case ETypeSource.Crystal:
                if (crystals + count > 0 && crystals + count < maxCrystals)
                    crystals += count;
                break;
            case ETypeSource.Energy:
                if (energy + count > 0 && energy + count < maxEnergy)
                    energy += count;
                break;
            case ETypeSource.Exp:
                exp += count;
                break;
        }
    }

    public int GetValue(ETypeSource type)
    {
        int result = -1;
        switch (type)
        {
            case ETypeSource.People:
                result = people;
                break;
            case ETypeSource.Money:
                result = money;
                break;
            case ETypeSource.Crystal:
                result = crystals;
                break;
            case ETypeSource.Energy:
                result = energy;
                break;
            case ETypeSource.Exp:
                result = exp;
                break;
        }
        return result;
    }

    public int GetMaxValue(ETypeSource type)
    {
        int result = -1;
        switch (type)
        {
            case ETypeSource.People:
                result = maxPeople;
                break;
            case ETypeSource.Money:
                result = maxMoney;
                break;
            case ETypeSource.Crystal:
                result = maxCrystals;
                break;
            case ETypeSource.Energy:
                result = maxEnergy;
                break;
        }
        return result;
    }
}
